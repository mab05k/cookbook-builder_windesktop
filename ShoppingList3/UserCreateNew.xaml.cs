﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ShoppingList3
{
    /// <summary>
    /// Interaction logic for UserCreateNew.xaml
    /// </summary>
    public partial class UserCreateNew : Window
    {
        public UserCreateNew()
        {
            InitializeComponent();
        }

        private bool CheckName(string input) // Ensures a name is input
        {
            bool name = true;
            if (input == "")
                name = false;
            if (input == null)
                name = false;

            return name;
        }

        private void Window_Initialized(object sender, EventArgs e)
        {
            FocusManager.SetFocusedElement(this, NewUserNameTextBox);
            this.DataContext = new User(100, "", DateTime.Now);
        }

        private void AddUserButton_Click(object sender, RoutedEventArgs e)
        {
            string userInput = NewUserNameTextBox.Text;

            if (CheckName(userInput))
            {
                string[] userName = NewUserNameTextBox.Text.Split(' ');
                string fullName = null;
                for (int i = 0; i < userName.Length; i++)
                {
                    userName[i] = userName[i].Substring(0, 1).ToUpper() + userName[i].Substring(1, (userName[i].Length - 1)).ToLower();
                    fullName += userName[i] + " ";
                }

                string query = string.Format("INSERT INTO Users (UserName,MemberSince) " +
                    "VALUES ('{0}','{1}')", fullName, DateTime.Now.ToShortDateString());

                DBManager dbm = new DBManager();
                dbm.Open();
                dbm.InsertIntoDatabase(query);
                dbm.Close();

                User user = (User)(this.DataContext);
                ((ShoppingListApp)Application.Current).User.Add(user);

                DialogResult = true;
            }
            else
            {
                MessageBox.Show("Must input a new user name.", "Invalid Name", MessageBoxButton.OK, MessageBoxImage.Asterisk);
            }
        }
    }
}
