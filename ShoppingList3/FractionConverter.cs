﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingList3
{
    public class FractionConverter
    {
        static string DecimalToFraction(string number)
        {
            decimal numerator2 = decimal.Parse(number) * 100;
            int numerator = (int)numerator2;
            decimal denominator = 100;

            for (int j = 0; j < denominator; j++)
            {
                for (int i = 2; i < denominator; i++)
                {
                    if (numerator % i == 0 && denominator % i == 0)
                    {
                        numerator /= i;
                        denominator /= i;
                    }
                }
            }

            return numerator + "/" + denominator;
        }
    }
}
