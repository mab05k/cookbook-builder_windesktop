﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ShoppingList3
{
    /// <summary>
    /// Interaction logic for WelcomePage.xaml
    /// </summary>
    public partial class WelcomePage : Page
    {
        public WelcomePage()
        {
            InitializeComponent();
        }

        private void AddUserButton_Click(object sender, RoutedEventArgs e)
        {
            UserCreateNew ucn = new UserCreateNew();
            ucn.ShowDialog();
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (ProfileComboBox.Text == "")
            {
                UserDelete ud = new UserDelete();
                ud.ShowDialog();
            }
            else
            {
                MessageBoxResult result = MessageBox.Show("Are you sure you want to delete \"" + ProfileComboBox.Text + "\"",
                    "Delete User", MessageBoxButton.YesNo, MessageBoxImage.Question);

                if (result == MessageBoxResult.Yes)
                {
                    // Removes user from the Database
                    string query = string.Format("DELETE FROM Users WHERE UserName = '{0}'", ProfileComboBox.Text);
                    DBManager dbm = new DBManager();
                    dbm.Open();
                    dbm.DeleteFromDatabase(query);
                    dbm.Close();

                    // Removes User from the List
                    ((ShoppingListApp)Application.Current).RemoveUser((User)ProfileComboBox.SelectedItem);
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (ProfileComboBox.Text != "")
            {
                ((ShoppingListApp)Application.Current).CurrentUser = (User)ProfileComboBox.SelectedItem;

                RecipePage rp = new RecipePage();
                this.NavigationService.Navigate(rp);
            }
        }        
    }
}
