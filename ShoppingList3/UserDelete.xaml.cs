﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ShoppingList3
{
    /// <summary>
    /// Interaction logic for UserDelete.xaml
    /// </summary>
    public partial class UserDelete : Window
    {
        public UserDelete()
        {
            InitializeComponent();
        }

        private bool CheckComboBox()
        {
            bool selected = true;
            if (ProfileComboBox.Text == "")
                selected = false;
            if (ProfileComboBox.Text == null)
                selected = false;

            return selected;
        }
        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (CheckComboBox())
            {
                // Removes user from the Database
                string query = string.Format("DELETE FROM Users WHERE UserName = '{0}'", ProfileComboBox.Text);
                DBManager dbm = new DBManager();
                dbm.Open();
                dbm.DeleteFromDatabase(query);
                dbm.Close();

                // Removes User from the List
                ((ShoppingListApp)Application.Current).RemoveUser((User)ProfileComboBox.SelectedItem);

                DialogResult = true;
            }
            else
            {
                MessageBox.Show("You must select a profile to delete.", "Invalid Selection", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
