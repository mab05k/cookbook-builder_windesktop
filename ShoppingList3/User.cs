﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingList3
{
    public class User : INotifyPropertyChanged
    {
        private int user_id;
        private string userName;
        private DateTime memberSince;

        public event PropertyChangedEventHandler PropertyChanged;

        public int User_id
        {
            get { return this.user_id; }
            set { this.user_id = value; }
        }
        public string UserName
        {
            get { return this.userName; }
            set 
            {
                this.userName = value;
                OnPropertyChanged("UserName"); 
            }
        }
        public DateTime MemberSince
        {
            get { return this.memberSince; }
            set { this.MemberSince = value.Date; }
        }

        public User(int user_id, string userName, DateTime memberSince)
        {
            this.user_id = user_id;
            this.userName = userName;
            this.memberSince = memberSince;
        }

        protected void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
