﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ShoppingList3
{
    /// <summary>
    /// Interaction logic for UserDeleteIngredient.xaml
    /// </summary>
    public partial class UserDeleteIngredient : Window
    {
        Recipe myRecipe;

        public UserDeleteIngredient()
        {
            InitializeComponent();
        }
        private void Window_Initialized(object sender, EventArgs e)
        {
            myRecipe = new Recipe(1, ((ShoppingListApp)Application.Current).CurrentUser.User_id, "", null, DateTime.Now, 0, "");
            myRecipe.Ingredients.Add(new Ingredient());
            this.DataContext = myRecipe;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (CheckInputs())
            {
                string confirmation = string.Format("Are you sure you want to delete the ingredient \"{0}\" from the category \"{1}\"?",
                    myRecipe.Ingredients[0].SelectedIngredientType, myRecipe.Ingredients[0].SelectedCategory);
                MessageBoxResult result = MessageBox.Show(confirmation, "Are you sure?", MessageBoxButton.YesNo,
                    MessageBoxImage.Question);

                if (result == MessageBoxResult.Yes)
                {
                    string ingredientType = GetIngredientType(myRecipe.Ingredients[0].SelectedCategory.Category);
                    string query = string.Format("DELETE FROM {0} WHERE {0}Name = '{1}'", ingredientType, myRecipe.Ingredients[0].SelectedIngredientType);
                    DBManager dbm = new DBManager();
                    dbm.Open();
                    dbm.DeleteFromDatabase(query);
                    dbm.Close();

                    this.Close();
                }
            }
            else
            {
                MessageBox.Show("You must fill out all fields.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }   
     
        private bool CheckInputs()
        {
            bool selected = true;

            if (myRecipe.Ingredients[0].SelectedCategory == null)
                selected = false;
            if (myRecipe.Ingredients[0].SelectedIngredientType == null)
                selected = false;

            return selected;
        }
        private string GetIngredientType(string selectedCategory)
        {
            string ingredientType = null;

            switch (myRecipe.Ingredients[0].SelectedCategory.Category)
            {
                case "Meat":
                    ingredientType = "Meat";
                    break;
                case "Vegetables":
                    ingredientType = "Vegetable";
                    break;
                case "Fruit":
                    ingredientType = "Fruit";
                    break;
                case "Nuts":
                    ingredientType = "Nut";
                    break;
                case "Dairy":
                    ingredientType = "Dairy";
                    break;
                case "Herbs & Spices":
                    ingredientType = "Herb";
                    break;
                case "Legumes":
                    ingredientType = "Legume";
                    break;
                case "Oils":
                    ingredientType = "Oil";
                    break;
                case "Vinegars & Stocks":
                    ingredientType = "VinegarStock";
                    break;
                case "Specialty Item":
                    ingredientType = "Specialty";
                    break;
                default:
                    break;
            }

            return ingredientType;
        }
    }
}
