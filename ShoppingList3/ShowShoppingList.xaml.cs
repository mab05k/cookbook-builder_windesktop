﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ShoppingList3
{
    /// <summary>
    /// Interaction logic for ShowShoppingList.xaml
    /// </summary>
    public partial class ShowShoppingList : Window, INotifyPropertyChanged
    {
        #region Properties, Getters, & Setters
        private ObservableCollection<Ingredient> meat;
        private ObservableCollection<Ingredient> vegetable;
        private ObservableCollection<Ingredient> fruit;
        private ObservableCollection<Ingredient> nuts;
        private ObservableCollection<Ingredient> dairy;
        private ObservableCollection<Ingredient> herbs;
        private ObservableCollection<Ingredient> legumes;
        private ObservableCollection<Ingredient> oils;
        private ObservableCollection<Ingredient> vinegarsstocks;
        private ObservableCollection<Ingredient> specialty;

        public ObservableCollection<Ingredient> Meat
        {
            get { return this.meat; }
            set 
            {
                this.meat = value;
                OnPropertyChanged("Meat");
            }
        }
        public ObservableCollection<Ingredient> Vegetable
        {
            get { return this.vegetable; }
            set
            {
                this.vegetable = value;
                OnPropertyChanged("Vegetable");
            }
        }
        public ObservableCollection<Ingredient> Fruit
        {
            get { return this.fruit; }
            set
            {
                this.fruit = value;
                OnPropertyChanged("Fruit");
            }
        }
        public ObservableCollection<Ingredient> Nuts
        {
            get { return this.nuts; }
            set
            {
                this.nuts = value;
                OnPropertyChanged("Nuts");
            }
        }
        public ObservableCollection<Ingredient> Dairy
        {
            get { return this.dairy; }
            set
            {
                this.dairy = value;
                OnPropertyChanged("Dairy");
            }
        }
        public ObservableCollection<Ingredient> Herbs
        {
            get { return this.herbs; }
            set
            {
                this.herbs = value;
                OnPropertyChanged("Herbs");
            }
        }
        public ObservableCollection<Ingredient> Legumes
        {
            get { return this.legumes; }
            set
            {
                this.legumes = value;
                OnPropertyChanged("Legumes");
            }
        }
        public ObservableCollection<Ingredient> Oils
        {
            get { return this.oils; }
            set
            {
                this.oils = value;
                OnPropertyChanged("Oils");
            }
        }
        public ObservableCollection<Ingredient> VinegarStocks
        {
            get { return this.vinegarsstocks; }
            set
            {
                this.vinegarsstocks = value;
                OnPropertyChanged("VinegarStocks");
            }
        }
        public ObservableCollection<Ingredient> Specialty
        {
            get { return this.specialty; }
            set
            {
                this.specialty = value;
                OnPropertyChanged("Specialty");
            }
        }
        #endregion
        
        private void Window_Initialized(object sender, EventArgs e)
        {
            string userName = ((ShoppingListApp)Application.Current).CurrentUser.UserName;
            string date = DateTime.Today.ToString("dd MMM yyyy");
            TitleTextBlock.Text = string.Format("{0}'s Shopping List", userName);
            DateTextBlock.Text = string.Format("{0}", date);
        }

        public MeasurementConversions Conversions;
        public event PropertyChangedEventHandler PropertyChanged;

        public ShowShoppingList(ObservableCollection<Recipe> recipes)
        {
            InitializeComponent();

            Meat = new ObservableCollection<Ingredient>();
            Vegetable = new ObservableCollection<Ingredient>();
            Fruit = new ObservableCollection<Ingredient>();
            Nuts = new ObservableCollection<Ingredient>();
            Dairy = new ObservableCollection<Ingredient>();
            Herbs = new ObservableCollection<Ingredient>();
            Legumes = new ObservableCollection<Ingredient>();
            Oils = new ObservableCollection<Ingredient>();
            VinegarStocks = new ObservableCollection<Ingredient>();
            Specialty = new ObservableCollection<Ingredient>();
            Conversions = new MeasurementConversions();

            PrintShoppingList(recipes);
            this.DataContext = this;
        }

        private void PrintShoppingList(ObservableCollection<Recipe> recipes)
        {
            ClearCollections();

            #region IngredientsLoop
            for (int i = 0; i < recipes.Count; i++) {
                for (int j = 0; j < recipes[i].Ingredients.Count; j++) {
                    switch (recipes[i].Ingredients[j].SelectedCategory.Category)
                    {
                        case "Meat":
                            AddIngredientsSpecific(Meat, recipes[i].Ingredients[j]);
                            break;
                        case "Vegetables":
                            AddIngredientsSpecific(Vegetable, recipes[i].Ingredients[j]);
                            break;
                        case "Fruit":
                            AddIngredientsSpecific(Fruit, recipes[i].Ingredients[j]);
                            break;
                        case "Nuts":
                            AddIngredientsSpecific(Nuts, recipes[i].Ingredients[j]);
                            break;
                        case "Dairy":
                            AddIngredientsSpecific(Dairy, recipes[i].Ingredients[j]);
                            break;
                        case "Herbs & Spices":
                            AddIngredientsGeneric(Herbs, recipes[i].Ingredients[j]);
                            break;
                        case "Legumes":
                            AddIngredientsSpecific(Legumes, recipes[i].Ingredients[j]);
                            break;
                        case "Oils":
                            AddIngredientsGeneric(Oils, recipes[i].Ingredients[j]);
                            break;
                        case "Vinegars & Stocks":
                            AddIngredientsGeneric(VinegarStocks, recipes[i].Ingredients[j]);
                            break;
                        case "Specialty Item":
                            AddIngredientsGeneric(Specialty, recipes[i].Ingredients[j]);
                            break;
                        default:
                            break;
                    }
                }
            }
            #endregion

            Meat = new ObservableCollection<Ingredient>(Meat.OrderBy(i => i.IngredientName));
            Vegetable = new ObservableCollection<Ingredient>(Vegetable.OrderBy(i => i.IngredientName));
            Fruit = new ObservableCollection<Ingredient>(Fruit.OrderBy(i => i.IngredientName));
            Nuts = new ObservableCollection<Ingredient>(Nuts.OrderBy(i => i.IngredientName));
            Dairy = new ObservableCollection<Ingredient>(Dairy.OrderBy(i => i.IngredientName));
            Herbs = new ObservableCollection<Ingredient>(Herbs.OrderBy(i => i.IngredientName));
            Legumes = new ObservableCollection<Ingredient>(Legumes.OrderBy(i => i.IngredientName));
            Oils = new ObservableCollection<Ingredient>(Oils.OrderBy(i => i.IngredientName));
            VinegarStocks = new ObservableCollection<Ingredient>(VinegarStocks.OrderBy(i => i.IngredientName));
            Specialty = new ObservableCollection<Ingredient>(Specialty.OrderBy(i => i.IngredientName));
        }
        private void ClearCollections()
        {
            Meat.Clear();
            Vegetable.Clear();
            Fruit.Clear();
            Nuts.Clear();
            Dairy.Clear();
            Herbs.Clear();
            Legumes.Clear();
            Oils.Clear();
            VinegarStocks.Clear();
            Specialty.Clear();
        }
        private void AddIngredientsSpecific(ObservableCollection<Ingredient> Food, Ingredient ingredient)
        {
            if (Food.Count == 0)
                Food.Add(new Ingredient(ingredient.Id, ingredient.Recipes_ID, ingredient.IngredientType, ingredient.IngredientName,
                    ingredient.IngredientMeasurement, ingredient.IngredientAmount));
            else {
                for (int i = 0; i < Food.Count; i++) {
                    if (Food[i].IngredientName == ingredient.IngredientName) {
                        if (Food[i].IngredientMeasurement == ingredient.IngredientMeasurement) {
                            Food[i].IngredientAmount = (double.Parse(Food[i].IngredientAmount) + double.Parse(ingredient.IngredientAmount)).ToString();
                            return;
                        }
                        #region LB:OZ
                        // LB:OZ
                        else if (Food[i].IngredientMeasurement == "Lb." && ingredient.IngredientMeasurement == "Oz.") {
                            Food[i].IngredientAmount = Conversions.OzToPound(Food[i].IngredientAmount, ingredient.IngredientAmount);
                            return;
                        } // OZ:LB
                        else if (Food[i].IngredientMeasurement == "Oz." && ingredient.IngredientMeasurement == "Lb.") {
                            Food[i].IngredientAmount = Conversions.OzToPound(ingredient.IngredientAmount, Food[i].IngredientAmount);
                            Food[i].IngredientMeasurement = "Lb.";
                            return;
                        }
                        #endregion
                        #region COUNT:CLOVE
                        // COUNT:CLOVE
                        else if (Food[i].IngredientMeasurement == "Count" && ingredient.IngredientMeasurement == "Clove")
                        {
                            Food[i].IngredientAmount = (double.Parse(Food[i].IngredientMeasurement) + 1).ToString();
                            return;
                        } // CLOVE:COUNT
                        else if (Food[i].IngredientMeasurement == "Clove" && ingredient.IngredientMeasurement == "Count")
                        {
                            Food[i].IngredientMeasurement = "Count";
                            Food[i].IngredientAmount = (double.Parse(ingredient.IngredientAmount) + 1).ToString();
                            return;
                        }
                        #endregion
                        #region COUNT:CUPS
                        // COUNT:CUPS
                        else if (Food[i].IngredientMeasurement == "Count" && ingredient.IngredientMeasurement == "Cups")
                        {
                            if (double.Parse(ingredient.IngredientAmount) >= 4) {
                                Food[i].IngredientAmount = (double.Parse(Food[i].IngredientAmount) + 1).ToString();
                                return;
                            }
                            else
                                break;
                        } // CUPS:COUNT
                        else if (Food[i].IngredientMeasurement == "Cups" && ingredient.IngredientMeasurement == "Count")
                        {
                            if (double.Parse(Food[i].IngredientAmount) >= 4) {
                                Food[i].IngredientMeasurement = "Count";
                                Food[i].IngredientAmount = (double.Parse(ingredient.IngredientAmount) + 1).ToString();
                                return;
                            }
                            else
                                break;
                        }
                        #endregion
                        #region COUNT:SLICE
                        // COUNT:SLICE
                        else if (Food[i].IngredientMeasurement == "Count" && ingredient.IngredientMeasurement == "Slice")
                        {
                            if (double.Parse(ingredient.IngredientAmount) >= 4)
                            {
                                Food[i].IngredientAmount = (double.Parse(Food[i].IngredientAmount) + 1).ToString();
                                return;
                            }
                            else
                                break;
                        } // SLICE:COUNT
                        else if (Food[i].IngredientMeasurement == "Slice" && ingredient.IngredientMeasurement == "Count")
                        {
                            if (double.Parse(Food[i].IngredientAmount) >= 4)
                            {
                                Food[i].IngredientMeasurement = "Count";
                                Food[i].IngredientAmount = (double.Parse(ingredient.IngredientAmount) + 1).ToString();
                                return;
                            }
                            else
                                break;
                        }
                        #endregion
                        #region GALLON:CUP
                        // GALLON:CUP
                        else if (Food[i].IngredientMeasurement == "Gallon" && ingredient.IngredientMeasurement == "Cup")
                        {
                            Food[i].IngredientAmount = Conversions.OzToPound(Food[i].IngredientAmount, ingredient.IngredientAmount);
                            return;
                        } // CUP:GALLON
                        else if (Food[i].IngredientMeasurement == "Cup" && ingredient.IngredientMeasurement == "Gallon")
                        {
                            Food[i].IngredientAmount = Conversions.OzToPound(ingredient.IngredientAmount, Food[i].IngredientAmount);
                            Food[i].IngredientMeasurement = "Gallon";
                            return;
                        }
                        #endregion
                        #region TBSP:TSP
                            // TBSP:TSP
                        else if (Food[i].IngredientMeasurement == "Tbsp" && ingredient.IngredientMeasurement == "Tsp")
                        {
                            Food[i].IngredientAmount = Conversions.TbspPlusTsp(Food[i].IngredientMeasurement, ingredient.IngredientMeasurement);
                            return;
                        } // TSP:TBSP
                        else if (Food[i].IngredientMeasurement == "Tsp" && ingredient.IngredientMeasurement == "Tbsp")
                        {
                            Food[i].IngredientAmount = Conversions.TbspPlusTsp(ingredient.IngredientMeasurement, Food[i].IngredientMeasurement);
                            Food[i].IngredientMeasurement = "Tbsp";
                            return;
                        }
                        #endregion
                    }
                }
                Food.Add(new Ingredient(ingredient.Id, ingredient.Recipes_ID, ingredient.IngredientType, ingredient.IngredientName,
            ingredient.IngredientMeasurement, ingredient.IngredientAmount));
            }
        }
        private void AddIngredientsGeneric(ObservableCollection<Ingredient> Food, Ingredient ingredient)
        {
            if (Food.Count == 0)
                Food.Add(new Ingredient(ingredient.Id, ingredient.Recipes_ID, ingredient.IngredientType, ingredient.IngredientName,
                    ingredient.IngredientMeasurement, ingredient.IngredientAmount));
            else
            {
                for (int i = 0; i < Food.Count; i++)
                    if (Food[i].IngredientName == ingredient.IngredientName)
                        return;

                Food.Add(new Ingredient(ingredient.Id, ingredient.Recipes_ID, ingredient.IngredientType, ingredient.IngredientName,
                    ingredient.IngredientMeasurement, ingredient.IngredientAmount));
            }
        }

        protected void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }        
    }
}
