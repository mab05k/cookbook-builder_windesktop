﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingList3
{
    public class DBManager
    {
        public string ConnString { get; set; }
        private SqlConnection Conn;

        public DBManager()
        {
            //ConnString = string.Format(@"Data Source=(LocalDB)\v11.0;" +
            //    "AttachDbFilename=\"C:\\5. Programming\\ShoppingList3\\ShoppingList3\\SLDataBase.mdf\";" +
            //    "Integrated Security=True");

            ConnString = string.Format(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=|DataDirectory|\SLDataBase.mdf;Integrated Security=True;");
            
            SqlConnection conn = new SqlConnection(ConnString);
            this.Conn = conn;
        }

        // Opens and closes the connection to DB
        public void Open()
        {
            Conn.Open();
        }
        public void Close()
        {
            Conn.Close();
        }

        // Generic interaction with the ShoppingListDatabase.mdf
        public DataTable SelectFromDatabase(string query)
        {
            SqlDataAdapter sda = new SqlDataAdapter(query, Conn);
            DataTable dt = new DataTable();
            sda.Fill(dt);

            return dt;
        }
        public void InsertIntoDatabase(string query)
        {
            using (SqlCommand cmd = new SqlCommand(query, Conn))
            {
                cmd.ExecuteNonQuery();
            }
        }
        public void DeleteFromDatabase(string query)
        {
            using (SqlCommand cmd = new SqlCommand(query, Conn))
            {
                cmd.ExecuteNonQuery();
            }
        }
        
        // Interaction specific to the Recipes table in CreateNewRecipeWindow.cs
        public void InsertIntoRecipe(string userID, string recipeName, string description)
        {
            string addRecipe = string.Format("INSERT INTO Recipes (Users_ID,RecipeName,RecipeImage,DateCreated,Visits,Description)" +
                "VALUES (@Users_ID,@RecipeName,'{0}','{1}','{2}',@Description)", null, DateTime.Now, 0);
            using (SqlCommand cmd = new SqlCommand(addRecipe, Conn))
            {
                cmd.Parameters.AddWithValue("@Users_ID", userID);
                cmd.Parameters.AddWithValue("@RecipeName", recipeName);
                cmd.Parameters.AddWithValue("@Description", description);

                cmd.ExecuteNonQuery();
            }
        }
        public void UpdateRecipe(string recipeID, string userID, string recipeName, string recipeImage, DateTime created, string visits, string description)
        {
            string updateRecipe = string.Format("UPDATE Recipes" +
                " SET Users_ID=@Users_ID,RecipeName=@RecipeName,RecipeImage='{0}',DateCreated=@DateCreated,Visits=@Visits,Description=@Description" +
                " WHERE Id = '{1}'", null, recipeID);
            using (SqlCommand cmd = new SqlCommand(updateRecipe, Conn))
            {
                cmd.Parameters.AddWithValue("@Users_ID", userID);
                cmd.Parameters.AddWithValue("@RecipeName", recipeName);
                //cmd.Parameters.AddWithValue("@RecipeImage", recipeImage);
                cmd.Parameters.AddWithValue("@DateCreated", created);
                cmd.Parameters.AddWithValue("@Visits", visits);
                cmd.Parameters.AddWithValue("@Description", description);

                cmd.ExecuteNonQuery();
            }
        }
        public DataTable SelectFromRecipe(int user_id, string recipeName)
        {
            //string query = string.Format("SELECT Id FROM Recipes WHERE RecipeName = @RecipeName", recipeName);
            string query = string.Format("SELECT Id FROM Recipes WHERE Users_ID = {0} AND RecipeName = @RecipeName", user_id, recipeName);

            using (SqlCommand cmd = new SqlCommand(query, Conn))
            {
                cmd.Parameters.AddWithValue("@RecipeName", recipeName);

                SqlDataReader reader = cmd.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Load(reader);
                return dt;
            }
        }

        public void InsertIntoIngredient(string recipe_id, string ingredientType, string ingredientName, string ingredientMeasurement, string ingredientAmount)
        {
            string addIngredient = string.Format("INSERT INTO Ingredients (Recipes_ID, IngredientType, IngredientName, IngredientMeasurement, IngredientAmount)" +
                    " VALUES (@Recipes_ID,@IngredientType,@IngredientName,@IngredientMeasurement,@IngredientAmount)");

            using (SqlCommand cmd = new SqlCommand(addIngredient, Conn))
            {
                cmd.Parameters.AddWithValue("@Recipes_ID", recipe_id);
                cmd.Parameters.AddWithValue("@IngredientType", ingredientType);
                cmd.Parameters.AddWithValue("@IngredientName", ingredientName);
                cmd.Parameters.AddWithValue("@IngredientMeasurement", ingredientMeasurement);
                cmd.Parameters.AddWithValue("@IngredientAmount", ingredientAmount);

                cmd.ExecuteNonQuery();
            }
        }
        public void UpdateIngredients(string ingredient_id, string recipe_id, string ingredientType, string ingredientName, string ingredientMeasurement, string ingredientAmount)
        {
            string updateIngredient = string.Format("UPDATE Ingredients" +
                        " SET Recipes_ID=@Recipes_ID,IngredientType=@IngredientType,IngredientName=@IngredientName," +
                        "IngredientMeasurement=@IngredientMeasurement,IngredientAmount=@IngredientAmount" +
                        " WHERE Id = '{0}'", ingredient_id);

            using (SqlCommand cmd = new SqlCommand(updateIngredient, Conn))
            {
                cmd.Parameters.AddWithValue("@Recipes_ID", recipe_id);
                cmd.Parameters.AddWithValue("@IngredientType", ingredientType);
                cmd.Parameters.AddWithValue("@IngredientName", ingredientName);
                cmd.Parameters.AddWithValue("@IngredientMeasurement", ingredientMeasurement);
                cmd.Parameters.AddWithValue("@IngredientAmount", ingredientAmount);

                cmd.ExecuteNonQuery();
            }
        }
    }
}
