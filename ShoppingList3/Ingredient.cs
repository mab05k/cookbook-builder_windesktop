﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ShoppingList3
{
    public class Ingredient : INotifyPropertyChanged
    {
        private ObservableCollection<FoodCategory> category;        
        private ObservableCollection<IngredientTypeSelection> ingredientTypeList;        
        private ObservableCollection<Measurement> measurement;

        private FoodCategory selectedCategory;
        private IngredientTypeSelection selectedIngredientType;
        private Measurement selectedMeasurement;
        private string selectedAmount;

        private string id;
        private string recipes_ID;
        private string ingredientType;
        private string ingredientName;
        private string ingredientMeasurement;
        private string ingredientAmount;

        public event PropertyChangedEventHandler PropertyChanged;

        #region Property Getters and Setters
        public ObservableCollection<FoodCategory> Category
        {
            get { return this.category; }
            set
            {
                this.category = value;
            }
        }
        public ObservableCollection<IngredientTypeSelection> IngredientTypeList
        {
            get { return this.ingredientTypeList; }
            set
            {
                this.ingredientTypeList = value;
                OnPropertyChanged("IngredientTypeList");
            }
        }
        public ObservableCollection<Measurement> Measurement
        {
            get { return this.measurement; }
            set 
            {
                this.measurement = value;
            }
        }

        public FoodCategory SelectedCategory
        {
            get { return this.selectedCategory; }
            set
            {
                this.selectedCategory = value;
                OnPropertyChanged("SelectedCategory");
                ClearIngredients();
                this.IngredientTypeList = GetIngredientType();
                FillMeasurement();
            }
        }
        public IngredientTypeSelection SelectedIngredientType
        {
            get { return this.selectedIngredientType; }
            set
            {
                this.selectedIngredientType = value;
                OnPropertyChanged("SelectedIngredientType");
            }
        }
        public Measurement SelectedMeasurement
        {
            get { return this.selectedMeasurement; }
            set
            {
                this.selectedMeasurement = value;
                OnPropertyChanged("SelectedMeasurement");
            }
        }
        public string SelectedAmount
        {
            get { return this.selectedAmount; }
            set
            {
                this.selectedAmount = value;
                OnPropertyChanged("SelectedAmount");
            }
        }

        public string Id 
        {
            get { return this.id; }
            set 
            {
                this.id = value;
                OnPropertyChanged("Id");
            }
        }
        public string Recipes_ID 
        {
            get { return this.recipes_ID; }
            set
            {
                this.recipes_ID = value;
                OnPropertyChanged("Recipes_ID");
            }
        }
        public string IngredientType 
        {
            get { return this.ingredientType; }
            set
            {
                this.ingredientType = value;
                OnPropertyChanged("IngredientType");
            }
        }
        public string IngredientName 
        {
            get { return this.ingredientName; }
            set
            {
                this.ingredientName = value;
                OnPropertyChanged("IngredientName");
            }
        }
        public string IngredientMeasurement {
            get { return this.ingredientMeasurement; }
            set
            {
                this.ingredientMeasurement = value;
                OnPropertyChanged("IngredientMeasurement");
            }
        }
        public string IngredientAmount 
        {
            get { return this.ingredientAmount; }
            set
            {
                this.ingredientAmount = value;
                OnPropertyChanged("IngredientAmount");
            }
        }
        #endregion

        public Ingredient()
        {
            Category = new ObservableCollection<FoodCategory>();
            Measurement = new ObservableCollection<ShoppingList3.Measurement>();
            IngredientTypeList = new ObservableCollection<IngredientTypeSelection>();

            FillCategories();
        }
        public Ingredient(string id, string recipes_id, string ingredientType, string ingredientName, string ingredientMeasurement, string ingredientAmount)
        {
            this.Id = id;
            this.Recipes_ID = recipes_id;
            this.IngredientType = ingredientType;
            this.IngredientName = ingredientName;
            this.IngredientMeasurement = ingredientMeasurement;
            this.IngredientAmount = ingredientAmount;

            Category = new ObservableCollection<FoodCategory>();
            Measurement = new ObservableCollection<ShoppingList3.Measurement>();
            IngredientTypeList = new ObservableCollection<IngredientTypeSelection>();

            FillCategories();

            for (int i = 0; i < Category.Count; i++)
            {
                if (Category[i].Category == ingredientType)
                    SelectedCategory = Category[i];
            }
            for (int i = 0; i < Measurement.Count; i++)
            {
                if (Measurement[i].Meas == ingredientMeasurement)
                    SelectedMeasurement = Measurement[i];
            }
            for (int i = 0; i < IngredientTypeList.Count; i++)
            {
                if (IngredientTypeList[i].IngredientType == ingredientName)
                    SelectedIngredientType = IngredientTypeList[i];
            }
            SelectedAmount = ingredientAmount;
        }

        protected void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
        
        private void FillCategories()
        {
            Category.Add(new FoodCategory("Meat"));
            Category.Add(new FoodCategory("Vegetables"));
            Category.Add(new FoodCategory("Fruit"));
            Category.Add(new FoodCategory("Nuts"));
            Category.Add(new FoodCategory("Dairy"));
            Category.Add(new FoodCategory("Herbs & Spices"));
            Category.Add(new FoodCategory("Oils"));
            Category.Add(new FoodCategory("Legumes"));
            Category.Add(new FoodCategory("Vinegars & Stocks"));
            Category.Add(new FoodCategory("Specialty Item"));
        }
        private void FillMeasurement()
        {
            switch (SelectedCategory.Category)
            {
                case "Meat":
                    Measurement.Add(new Measurement("Lb."));
                    Measurement.Add(new Measurement("Oz."));
                    break;
                case "Vegetables":
                    Measurement.Add(new Measurement("Count"));
                    Measurement.Add(new Measurement("Clove"));
                    Measurement.Add(new Measurement("Cups"));
                    Measurement.Add(new Measurement("Lb."));
                    break;
                case "Fruit":
                    Measurement.Add(new Measurement("Count"));
                    Measurement.Add(new Measurement("Slice"));
                    Measurement.Add(new Measurement("Cups"));
                    break;
                case "Nuts":
                    Measurement.Add(new Measurement("Count"));
                    Measurement.Add(new Measurement("Lb."));
                    Measurement.Add(new Measurement("Oz."));                    
                    break;
                case "Dairy":
                    Measurement.Add(new Measurement("Lb."));
                    Measurement.Add(new Measurement("Oz."));                    
                    Measurement.Add(new Measurement("Gallon"));
                    Measurement.Add(new Measurement("Cup"));
                    Measurement.Add(new Measurement("Tbsp"));
                    Measurement.Add(new Measurement("Tsp"));
                    break;
                case "Herbs & Spices":
                    Measurement.Add(new Measurement("Sprig"));
                    Measurement.Add(new Measurement("Bunch"));
                    Measurement.Add(new Measurement("Tbsp"));
                    Measurement.Add(new Measurement("Tsp"));
                    Measurement.Add(new Measurement("Oz."));
                    break;
                case "Legumes":
                    Measurement.Add(new Measurement("Count"));
                    Measurement.Add(new Measurement("Cup"));
                    Measurement.Add(new Measurement("Lb."));
                    Measurement.Add(new Measurement("Oz."));
                    break;
                case "Oils":
                    Measurement.Add(new Measurement("Cup"));
                    Measurement.Add(new Measurement("Tbsp"));
                    Measurement.Add(new Measurement("Tsp"));
                    break;
                case "Vinegars & Stocks":
                    Measurement.Add(new Measurement("Cup"));
                    Measurement.Add(new Measurement("Tbsp"));
                    Measurement.Add(new Measurement("Tsp"));
                    break;
                case "Specialty Item":
                    Measurement.Add(new Measurement("Count"));
                    Measurement.Add(new Measurement("Lb."));
                    Measurement.Add(new Measurement("Oz."));
                    Measurement.Add(new Measurement("Cup"));
                    Measurement.Add(new Measurement("Tbsp"));
                    Measurement.Add(new Measurement("Tsp"));
                    break;
                default:
                    break;
            }
        }
        
        private void ClearIngredients() // Clears ingredient combobox and Measurement ComboBox
        {
            IngredientTypeList.Clear();
            Measurement.Clear();
        }
        private void FillIngredients(string category)
        {
            string query = string.Format("SELECT {0}Name FROM {0}", category);
            DBManager dbm = new DBManager();
            dbm.Open();
            var dt = dbm.SelectFromDatabase(query);
            dbm.Close();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                IngredientTypeList.Add(new IngredientTypeSelection(dt.Rows[i].ItemArray[0].ToString()));
            }

            IngredientTypeList = new ObservableCollection<IngredientTypeSelection>(IngredientTypeList.OrderBy(i => i.IngredientType.ToString()));
        }
        private ObservableCollection<IngredientTypeSelection> GetIngredientType()
        {
            if (SelectedCategory.Category == "Meat")
                FillIngredients("Meat");
            if (SelectedCategory.Category == "Vegetables")
                FillIngredients("Vegetable");
            if (SelectedCategory.Category == "Fruit")
                FillIngredients("Fruit");
            if (SelectedCategory.Category == "Nuts")
                FillIngredients("Nut");
            if (SelectedCategory.Category == "Dairy")
                FillIngredients("Dairy");
            if (SelectedCategory.Category == "Herbs & Spices")
                FillIngredients("Herb");
            if (SelectedCategory.Category == "Legumes")
                FillIngredients("Legume");
            if (SelectedCategory.Category == "Oils")
                FillIngredients("Oil");
            if (SelectedCategory.Category == "Vinegars & Stocks")
                FillIngredients("VinegarStock");
            if (SelectedCategory.Category == "Specialty Item")
                FillIngredients("Specialty");

            return IngredientTypeList;
        }
    }

    public class FoodCategory
    {
        public string Category { get; set; }
        public FoodCategory(string category)
        {
            this.Category = category;
        }
        public override string ToString()
        {
            return string.Format("{0}", Category);
        }
    }
    public class IngredientTypeSelection
    {
        public string IngredientType { get; set; }
        public IngredientTypeSelection(string ingredientType)
        {
            this.IngredientType = ingredientType;
        }
        public override string ToString()
        {
            return string.Format("{0}", IngredientType);
        }
    }
    public class Measurement
    {
        public string Meas { get; set; }
        public Measurement(string measurement)
        {
            this.Meas = measurement;
        }
        public override string ToString()
        {
            return string.Format("{0}", Meas);
        }
    }
}


