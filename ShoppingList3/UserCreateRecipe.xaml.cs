﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ShoppingList3
{
    /// <summary>
    /// Interaction logic for UserCreateRecipe.xaml
    /// </summary>
    public partial class UserCreateRecipe : Window
    {
        public Recipe TheRecipe;
        public RecipeList RecipeList;

        public UserCreateRecipe(RecipeList recipeList)
        {
            this.RecipeList = recipeList;
            InitializeComponent();
        }

        private void Window_Initialized(object sender, EventArgs e) // Sets the DataContext and adds the first ingredient
        {
            int id = 1;

            TheRecipe = new Recipe(id, ((ShoppingListApp)Application.Current).CurrentUser.User_id, "", null, DateTime.Now, 0, "");
            TheRecipe.Ingredients.Add(new Ingredient());
            this.DataContext = TheRecipe;
        }

        private void RemoveRowButton_Click(object sender, RoutedEventArgs e) // Removes a row
        {
            int ingredients = TheRecipe.Ingredients.Count;
            TheRecipe.Ingredients.RemoveAt(ingredients-1);
        }
        private void AddRowButton_Click(object sender, RoutedEventArgs e) // Adds a row
        {
            TheRecipe.Ingredients.Add(new Ingredient());
        }

        private bool CheckInt(string theInt)
        {
            try
            {
                int x = int.Parse(theInt);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        private bool CheckInputData(Recipe recipe)
        {
            bool noErrors = true;
            int errorCount = 0;
            int ingredientErrorCount = 0;
            string errorMessage = null;

            if (RecipeNameTextBox.Text == "Recipe Name")
            {
                errorCount++;
                errorMessage += "You must input a Recipe Name.\n";
                noErrors = false;
            }

            for (int i = 0; i < recipe.Ingredients.Count; i++)
            {
                if (recipe.Ingredients[i].SelectedCategory == null)                
                    ingredientErrorCount++;
                if (recipe.Ingredients[i].SelectedIngredientType == null)
                    ingredientErrorCount++;
                if (recipe.Ingredients[i].SelectedMeasurement == null)
                    ingredientErrorCount++;
                if (recipe.Ingredients[i].SelectedAmount == null)
                    ingredientErrorCount++;

                
            }                

            if (ingredientErrorCount > 0)
            {
                errorMessage += "You must fill all Ingredient fields.";
                noErrors = false;
            }

            if (ingredientErrorCount > 0 || errorCount > 0)
                MessageBox.Show(errorMessage, "Errors", MessageBoxButton.OK, MessageBoxImage.Error);

            return noErrors;
        }
        private void OKButton_Click(object sender, RoutedEventArgs e) // Add recipe to database - close window - refresh Recipe Page
        {
            Recipe recipe = (Recipe)(this.DataContext);
            recipe.RecipeName = RecipeNameTextBox.Text;
            recipe.Description = DescriptionTextBox.Text;
            int currentUserID = ((ShoppingListApp)Application.Current).CurrentUser.User_id;

            if (CheckInputData(recipe))
            {
                DBManager dbm = new DBManager();
                dbm.Open();
                dbm.InsertIntoRecipe(currentUserID.ToString(), RecipeNameTextBox.Text, DescriptionTextBox.Text); // Insert New Recipe Into the Database
                var dt = dbm.SelectFromRecipe(currentUserID, RecipeNameTextBox.Text);
                recipe.Id = (int)dt.Rows[0].ItemArray[0];

                for (int i = 0; i < recipe.Ingredients.Count; i++)
                {
                    string addIngredient = string.Format("INSERT INTO Ingredients (Recipes_ID, IngredientType, IngredientName, IngredientMeasurement, IngredientAmount)" +
                        " VALUES ('{0}','{1}','{2}','{3}','{4}')", (int)dt.Rows[0].ItemArray[0], recipe.Ingredients[i].SelectedCategory, recipe.Ingredients[i].SelectedIngredientType,
                        recipe.Ingredients[i].SelectedMeasurement, recipe.Ingredients[i].SelectedAmount);
                    dbm.InsertIntoDatabase(addIngredient);

                    recipe.Ingredients[i].IngredientType = recipe.Ingredients[i].SelectedCategory.Category;
                    recipe.Ingredients[i].IngredientName = recipe.Ingredients[i].SelectedIngredientType.IngredientType;
                    recipe.Ingredients[i].IngredientMeasurement = recipe.Ingredients[i].SelectedMeasurement.Meas;
                    recipe.Ingredients[i].IngredientAmount = recipe.Ingredients[i].SelectedAmount;

                    string queryID = string.Format("SELECT Id FROM Ingredients WHERE Recipes_ID = '{0}' AND IngredientName = '{1}'", 
                        recipe.Id, recipe.Ingredients[i].IngredientName);
                    var ingredientID = dbm.SelectFromDatabase(queryID);
                    recipe.Ingredients[i].Id = ingredientID.Rows[0].ItemArray[0].ToString();
                }
                dbm.Close();

                RecipeList.Recipes.Add(recipe);
                this.Close();
            }
        }

        private void DescriptionTextBox_GotFocus(object sender, RoutedEventArgs e) // Clears textbox for user description input
        {
            DescriptionTextBox.Text = "";
            DescriptionTextBox.Foreground = Brushes.White;
            DescriptionTextBox.FontStyle = FontStyles.Normal;
        }
        private void DescriptionTextBox_LostFocus(object sender, RoutedEventArgs e) // 
        {
            if (DescriptionTextBox.Text == "")
            {
                DescriptionTextBox.Text = "Recipe Description...";
                DescriptionTextBox.Foreground = new SolidColorBrush(Color.FromRgb(128, 128, 128));
                DescriptionTextBox.FontStyle = FontStyles.Italic;
            }
        }
        private void RecipeNameTextBox_GotFocus(object sender, RoutedEventArgs e) // Clears textbox for RecipeName entry
        {
            RecipeNameTextBox.Text = "";
            RecipeNameTextBox.Foreground = Brushes.White;
            RecipeNameTextBox.FontStyle = FontStyles.Normal;
        }
        private void RecipeNameTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (RecipeNameTextBox.Text == "")
            {
                RecipeNameTextBox.Text = "Recipe Name";
                RecipeNameTextBox.Foreground = new SolidColorBrush(Color.FromRgb(128, 128, 128));
                RecipeNameTextBox.FontStyle = FontStyles.Italic;
            }
        }

        private void AddIngredientButton_Click(object sender, RoutedEventArgs e)
        {
            UserCreateIngredient uci = new UserCreateIngredient();
            uci.Show();
        }
    }
}
