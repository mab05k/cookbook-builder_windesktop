﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingList3
{
    public class Recipe : INotifyPropertyChanged
    {
        private int id;
        private int users_id;
        private string recipeName;
        private byte[] image;
        private DateTime dateCreated;
        private int visits;
        private string description;
        private ObservableCollection<Ingredient> ingredients;

        public event PropertyChangedEventHandler PropertyChanged;

        #region Properties Getters and Setters
        public int Id
        {
            get { return this.id; }
            set
            {
                this.id = value;
            }
        }
        public int Users_Id
        {
            get { return this.users_id; }
            set
            {
                this.users_id = value;
            }
        }
        public string RecipeName
        {
            get { return this.recipeName; }
            set
            {
                this.recipeName = value;
                OnPropertyChanged("RecipeName");
            }
        }
        public byte[] Image
        {
            get { return this.image; }
            set
            {
                this.image = value;
                OnPropertyChanged("Image");
            }
        }
        public DateTime DateCreated
        {
            get { return this.dateCreated; }
            set
            {
                this.dateCreated = value;
            }
        }
        public int Visits
        {
            get { return this.visits; }
            set
            {
                this.visits = value;
                OnPropertyChanged("Visits");
            }
        }
        public string Description
        {
            get { return this.description; }
            set
            {
                this.description = value;
                OnPropertyChanged("Description");
            }
        }

        public ObservableCollection<Ingredient> Ingredients
        {
            get { return this.ingredients; }
            set { this.ingredients = value; }
        }
        #endregion

        public Recipe(int id, int users_id, string recipeName, byte[] image, DateTime dateCreated, int visits, string description)
        {
            this.id = id;
            this.users_id = users_id;
            this.recipeName = recipeName;
            this.image = image;
            this.dateCreated = dateCreated;
            this.visits = visits;
            this.description = description;
            this.ingredients = new ObservableCollection<Ingredient>();
        }

        private ObservableCollection<Ingredient> AddIngredient()
        {
            ObservableCollection<Ingredient> ingredientCollection = new ObservableCollection<Ingredient>();
            DBManager dbm = new DBManager();
            string getIngredients = string.Format("SELECT * FROM Ingredients WHERE Recipes_ID = {0}", Id);
            dbm.Open();
            var dt = dbm.SelectFromDatabase(getIngredients);
            dbm.Close();

            if (dt != null)
            {

            }

            return ingredientCollection;
        }
        protected void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
        public override string ToString()
        {
            return string.Format(RecipeName);
        }
    }
}
