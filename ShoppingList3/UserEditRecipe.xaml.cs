﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ShoppingList3
{
    /// <summary>
    /// Interaction logic for UserEditRecipe.xaml
    /// </summary>
    public partial class UserEditRecipe : Window
    {
        public RecipeList RecipeList;
        public Recipe theRecipe;

        public UserEditRecipe(RecipeList recipeList)
        {
            this.RecipeList = recipeList;
            InitializeComponent();            
        }

        private void Window_Initialized(object sender, EventArgs e) // Sets the DataContext and adds the first ingredient
        {
            theRecipe = new Recipe(RecipeList.SelectedRecipe.Id, RecipeList.SelectedRecipe.Users_Id, RecipeList.SelectedRecipe.RecipeName,
                RecipeList.SelectedRecipe.Image, RecipeList.SelectedRecipe.DateCreated, RecipeList.SelectedRecipe.Visits, RecipeList.SelectedRecipe.Description);
            for (int i = 0; i < RecipeList.SelectedRecipe.Ingredients.Count; i++) {
                theRecipe.Ingredients.Add(RecipeList.SelectedRecipe.Ingredients[i]);
            }
            this.DataContext = theRecipe;
        }

        private void RemoveRowButton_Click(object sender, RoutedEventArgs e) // Removes a row
        {
            int ingredients = theRecipe.Ingredients.Count;
            theRecipe.Ingredients.RemoveAt(ingredients - 1);
        }
        private void AddRowButton_Click(object sender, RoutedEventArgs e) // Adds a row
        {
            theRecipe.Ingredients.Add(new Ingredient());
        }

        private bool CheckInputData(Recipe recipe)
        {
            bool noErrors = true;
            int errorCount = 0;
            int ingredientErrorCount = 0;
            string errorMessage = null;

            if (RecipeNameTextBox.Text == "Recipe Name")
            {
                errorCount++;
                errorMessage += "You must give a unique Recipe Name.\n";
                noErrors = false;
            }

            for (int i = 0; i < recipe.Ingredients.Count; i++)
            {
                if (recipe.Ingredients[i].SelectedCategory == null)
                    ingredientErrorCount++;
                if (recipe.Ingredients[i].SelectedIngredientType == null)
                    ingredientErrorCount++;
                if (recipe.Ingredients[i].SelectedMeasurement == null)
                    ingredientErrorCount++;
                if (recipe.Ingredients[i].SelectedAmount == null)
                    ingredientErrorCount++;
            }

            if (ingredientErrorCount > 0)
            {
                errorMessage += "You must fill in all Ingredient fields.";
                noErrors = false;
            }

            if (ingredientErrorCount > 0 || errorCount > 0)
                MessageBox.Show(errorMessage, "Errors", MessageBoxButton.OK, MessageBoxImage.Error);

            return noErrors;
        }
        private void OKButton_Click(object sender, RoutedEventArgs e) // Update recipe to database - close window - refresh Recipe Page
        {
            Recipe recipe = (Recipe)(this.DataContext); // Need to get ingredients to include Ingredients.Id
            recipe.RecipeName = RecipeNameTextBox.Text;
            recipe.Description = DescriptionTextBox.Text;
            int currentUserID = ((ShoppingListApp)Application.Current).CurrentUser.User_id;
            List<Ingredient> newIngredients = new List<Ingredient>();

            if (CheckInputData(recipe))
            {
                int lastId = 0;
                Recipe updateRecipe = RecipeList.Recipes.FirstOrDefault(x => x.Id == recipe.Id);
                updateRecipe.RecipeName = recipe.RecipeName;
                updateRecipe.Description = recipe.Description;

                DBManager dbm = new DBManager();
                dbm.Open();
                dbm.UpdateRecipe(recipe.Id.ToString(), currentUserID.ToString(), recipe.RecipeName.ToString(), 
                    null, recipe.DateCreated, recipe.Visits.ToString(), recipe.Description);
                
                string ings = string.Format("SELECT * FROM Ingredients WHERE Recipes_ID = {0}", recipe.Id);
                var dt = dbm.SelectFromDatabase(ings);

                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows.Count <= recipe.Ingredients.Count)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            Ingredient updateIngredient = recipe.Ingredients.FirstOrDefault(x => x.Id == dt.Rows[i].ItemArray[0].ToString());
                            updateIngredient.IngredientType = recipe.Ingredients[i].SelectedCategory.Category;
                            updateIngredient.IngredientName = recipe.Ingredients[i].SelectedIngredientType.IngredientType;
                            updateIngredient.IngredientMeasurement = recipe.Ingredients[i].SelectedMeasurement.Meas;
                            updateIngredient.IngredientAmount = recipe.Ingredients[i].SelectedAmount;

                            dbm.UpdateIngredients(dt.Rows[i].ItemArray[0].ToString(),
                                recipe.Id.ToString(),
                                recipe.Ingredients[i].SelectedCategory.Category,
                                recipe.Ingredients[i].SelectedIngredientType.IngredientType,
                                recipe.Ingredients[i].SelectedMeasurement.Meas,
                                recipe.Ingredients[i].SelectedAmount);
                        }
                    }
                    if (dt.Rows.Count > recipe.Ingredients.Count)
                    {
                        int rowIndex = recipe.Ingredients.Count - 1;
                        lastId = (int)dt.Rows[rowIndex].ItemArray[0];
                        string delete = string.Format("DELETE FROM Ingredients WHERE Id > {0}", lastId);
                        dbm.DeleteFromDatabase(delete);
                    }
                }

                for (int i = dt.Rows.Count; i < recipe.Ingredients.Count; i++)
                {
                    string addIngredient = string.Format("INSERT INTO Ingredients (Recipes_ID, IngredientType, IngredientName, IngredientMeasurement, IngredientAmount)" +
                    " VALUES ('{0}','{1}','{2}','{3}','{4}')", recipe.Id, recipe.Ingredients[i].SelectedCategory, recipe.Ingredients[i].SelectedIngredientType,
                    recipe.Ingredients[i].SelectedMeasurement, recipe.Ingredients[i].SelectedAmount);
                    dbm.InsertIntoDatabase(addIngredient);

                    recipe.Ingredients[i].IngredientType = recipe.Ingredients[i].SelectedCategory.Category;
                    recipe.Ingredients[i].IngredientName = recipe.Ingredients[i].SelectedIngredientType.IngredientType;
                    recipe.Ingredients[i].IngredientMeasurement = recipe.Ingredients[i].SelectedMeasurement.Meas;
                    recipe.Ingredients[i].IngredientAmount = recipe.Ingredients[i].SelectedAmount;
                }
                dbm.Close();
                RecipeList.SelectedRecipe = theRecipe;
                this.Close();
            }
        }
    }
}
