﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ShoppingList3
{
    /// <summary>
    /// Interaction logic for RecipePage.xaml
    /// </summary>
    public partial class RecipePage : Page
    {
        private RecipeList RecipeList;

        public RecipePage()
        {
            InitializeComponent();
            RecipeList rl = new RecipeList();
            rl.LoadRecipeData(((ShoppingListApp)Application.Current).CurrentUser);
            this.RecipeList = rl;
            this.DataContext = rl;
            ((ShoppingListApp)Application.Current).MainWindow.Title = ((ShoppingListApp)Application.Current).CurrentUser.UserName +
                "\'s Recipes";
        }
        
        private void HomeButton_Click(object sender, RoutedEventArgs e) // Navigates to Welcome Page
        {
            WelcomePage wp = new WelcomePage();
            this.NavigationService.Navigate(wp);
        }        
        private void CreateRecipeButton_Click(object sender, RoutedEventArgs e) // Create a new recipe
        {
            UserCreateRecipe ucr = new UserCreateRecipe(RecipeList);
            ucr.ShowDialog();
        }
        private void EditRecipeButton_Click(object sender, RoutedEventArgs e)
        {
            UserEditRecipe uer = new UserEditRecipe(this.RecipeList);
            uer.ShowDialog();
        }
        private void DeleteRecipeButton_Click(object sender, RoutedEventArgs e)
        {
            string confirmation = string.Format("Are you sure you want to delete the recipe \"{0}\"", RecipeList.SelectedRecipe.RecipeName);
            MessageBoxResult result = MessageBox.Show(confirmation, "Delete Recipe", MessageBoxButton.YesNo, 
                MessageBoxImage.Question);

            if (result == MessageBoxResult.Yes)
            {
                string removeIngredients = string.Format("DELETE FROM Ingredients WHERE Recipes_ID = {0}", this.RecipeList.SelectedRecipe.Id);
                string removeRecipes = string.Format("DELETE FROM Recipes WHERE Id = {0}",
                    this.RecipeList.SelectedRecipe.Id);

                DBManager dbm = new DBManager();
                dbm.Open();
                dbm.DeleteFromDatabase(removeIngredients);
                dbm.DeleteFromDatabase(removeRecipes);
                dbm.Close();
                int selRecipe = this.RecipeList.Recipes.IndexOf(this.RecipeList.SelectedRecipe);
                this.RecipeList.Recipes.RemoveAt(selRecipe);
            }
        }
        private void CreateIngredientButton_Click(object sender, RoutedEventArgs e)
        {
            UserCreateIngredient uci = new UserCreateIngredient();
            uci.ShowDialog();
        }
        private void DeleteIngredientButton_Click(object sender, RoutedEventArgs e)
        {
            UserDeleteIngredient udi = new UserDeleteIngredient();
            udi.Show();
        } 

        private void RecipeListComboBox_DropDownClosed(object sender, EventArgs e)
        {
            RecipeList.SelectedRecipe = (Recipe)RecipeListComboBox.SelectedItem;
            RecipeSelected();
        }

        private void RecipeSelected()
        {
            try
            {
                if (RecipeListComboBox.Text != "")
                {
                    int id = RecipeList.SelectedRecipe.Id;
                    string query = string.Format("SELECT Visits FROM Recipes WHERE Id = {0}", id);

                    DBManager dbm = new DBManager();
                    dbm.Open();
                    var dt = dbm.SelectFromDatabase(query);
                    int visits = (int)dt.Rows[0].ItemArray[0];
                    visits++;
                    string updateVisits = string.Format("UPDATE Recipes SET Visits = {0} WHERE Id = {1}", visits, id);
                    dbm.InsertIntoDatabase(updateVisits);
                    dbm.Close();
                }
            }
            catch (Exception)
            {
                // do nothing...
            }
        }

        private void checkbox_Checked(object sender, RoutedEventArgs e)
        {
            Recipe recipe = ((CheckBox)sender).DataContext as Recipe;
            RecipeList.ShoppingList.Add(recipe);            
        }
        private void checkbox_Unchecked(object sender, RoutedEventArgs e)
        {
            Recipe recipe = ((CheckBox)sender).DataContext as Recipe;
            RecipeList.ShoppingList.Remove(recipe);
        }

        private void CreateShoppingListButton_Click(object sender, RoutedEventArgs e)
        {
            if (RecipeList.ShoppingList.Count > 0)
            {
                ShowShoppingList ssl = new ShowShoppingList(RecipeList.ShoppingList);
                ssl.Show();
            }
        }               
    }
}
