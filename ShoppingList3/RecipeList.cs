﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingList3
{
    public class RecipeList : INotifyPropertyChanged
    {
        private ObservableCollection<Recipe> recipes = new ObservableCollection<Recipe>();
        private ObservableCollection<Recipe> selectedRecipeView = new ObservableCollection<Recipe>();
        private ObservableCollection<Recipe> shoppingList = new ObservableCollection<Recipe>();
        private Recipe selectedRecipe;
        public User CurrentUser { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public ObservableCollection<Recipe> Recipes
        {
            get { return this.recipes; }
            set
            {
                this.recipes = value;
                OnPropertyChanged("Recipes");                
            }
        }
        public ObservableCollection<Recipe> SelectedRecipeView
        {
            get { return this.selectedRecipeView; }
            set
            {
                this.selectedRecipeView = value;
                OnPropertyChanged("SelectedRecipeView");
            }
        }
        public ObservableCollection<Recipe> ShoppingList
        {
            get { return this.shoppingList; }
            set
            {
                this.shoppingList = value;
                OnPropertyChanged("ShoppingList");
            }
        }
        public Recipe SelectedRecipe
        {
            get { return this.selectedRecipe; }
            set
            {
                this.selectedRecipe = value;                
                AddSelectedRecipeIngredients();
                AddSelectedRecipe();                      
                OnPropertyChanged("SelectedRecipes");
            }
        }

        public void LoadRecipeData(User user) // Loads Recipes based on User
        {
            CurrentUser = user;
            string query = string.Format("SELECT * FROM Recipes WHERE Users_ID = {0}", user.User_id);
            DBManager dbm = new DBManager();
            dbm.Open();
            var dt = dbm.SelectFromDatabase(query);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                // TODO: Account for null byte[]
                this.Recipes.Add(new Recipe((int)dt.Rows[i].ItemArray[0], int.Parse(dt.Rows[i].ItemArray[1].ToString()), dt.Rows[i].ItemArray[2].ToString(), null,
                                (DateTime)dt.Rows[i].ItemArray[4], (int)dt.Rows[i].ItemArray[5], dt.Rows[i].ItemArray[6].ToString()));
                string ings = string.Format("SELECT * FROM Ingredients WHERE Recipes_ID = {0}", dt.Rows[i].ItemArray[0]);
                var table = dbm.SelectFromDatabase(ings);
                for (int j = 0; j < table.Rows.Count; j++)
                {
                    this.Recipes[i].Ingredients.Add(new Ingredient(table.Rows[j].ItemArray[0].ToString(), table.Rows[j].ItemArray[1].ToString(), table.Rows[j].ItemArray[2].ToString(),
                        table.Rows[j].ItemArray[3].ToString(), table.Rows[j].ItemArray[4].ToString(), table.Rows[j].ItemArray[5].ToString()));
                }
            }
            dbm.Close();
            Recipes = new ObservableCollection<Recipe>(Recipes.OrderByDescending(i => i.Visits));
        }
        private void AddSelectedRecipeIngredients()
        {
            DBManager dbm = new DBManager();
            
            if (SelectedRecipe != null)
            {
                dbm.Open();
                DataTable recipe_ID = dbm.SelectFromRecipe(CurrentUser.User_id, SelectedRecipe.RecipeName);
                string getIngredients = string.Format("SELECT * FROM Ingredients WHERE Recipes_ID = {0}", recipe_ID.Rows[0].ItemArray[0]);
                var ingredients = dbm.SelectFromDatabase(getIngredients);
                dbm.Close();

                if (SelectedRecipe.Ingredients.Count == 0)
                {
                    for (int i = 0; i < ingredients.Rows.Count; i++)
                    {
                        SelectedRecipe.Ingredients.Add(new Ingredient(ingredients.Rows[i].ItemArray[0].ToString(), ingredients.Rows[i].ItemArray[1].ToString(), ingredients.Rows[i].ItemArray[2].ToString(),
                            ingredients.Rows[i].ItemArray[3].ToString(), ingredients.Rows[i].ItemArray[4].ToString(), ingredients.Rows[i].ItemArray[5].ToString()));
                    }
                }
            }
        }
        private void AddSelectedRecipe()
        {
            SelectedRecipeView.Clear();
            SelectedRecipeView.Add(SelectedRecipe); 
        }

        protected void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }        
    }
}
