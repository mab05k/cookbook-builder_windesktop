﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace ShoppingList3
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class ShoppingListApp : Application, INotifyPropertyChanged
    {
        private User currentUser;
        private ObservableCollection<User> user = new ObservableCollection<User>();

        public event PropertyChangedEventHandler PropertyChanged;

        void AppStartup(object sender, StartupEventArgs args)
        {
            LoadUserData();
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
        }

        public User CurrentUser
        {
            get { return this.currentUser; }
            set
            {
                this.currentUser = value;
            }
        }
        public ObservableCollection<User> User
        {
            get { return this.user; }
            set { this.user = value; }
        }

        protected void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }
        private void LoadUserData()
        {
            string query = "SELECT * FROM Users ORDER BY Id";
            DBManager dbm = new DBManager();
            dbm.Open();
            var dt = dbm.SelectFromDatabase(query);
            dbm.Close();

            for (int i = 0; i < dt.Rows.Count; i++) {
                this.User.Add(new User((int)dt.Rows[i].ItemArray[0], dt.Rows[i].ItemArray[1].ToString(), (DateTime)dt.Rows[i].ItemArray[2]));
            }
        }

        public void RemoveUser(User user) // Method to remove user from the collection
        {
            User.Remove(user);
        }
    }
}
